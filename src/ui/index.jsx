export { default as App } from './App';
export { default as Start } from './Start';
export { default as Game } from './Game';
export { default as Congratulation } from './Congratulation';
